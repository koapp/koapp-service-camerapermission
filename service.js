(function () {
    angular
      .module('king.services.camerapermission', [])
      .run(loadFunction);
  
    loadFunction.$inject = ['configService'];
  
    function loadFunction(configService) {
      // Register upper level modules
      try {
        if (configService.services && configService.services.camerapermission) {
          askpermissionFunction(configService.services.camerapermission.scope);
        } else {
          throw "The service is not added to the application";
        }
      } catch (error) {
        console.error("Error", error);
      }
    }
  
    function askpermissionFunction(scopeData) {
        if (!cordova) return;
        var deniedCount = 0;
     
        function onError(error) {
            console.error("The following error occurred: " + error);
        }
  
        function evaluateAuthorizationStatus(status) {
            console.log("STATUS: "+status);
            switch (status) {
            case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                console.log("Permission not requested");
                requestAuthorization();
                break;
            case cordova.plugins.diagnostic.permissionStatus.DENIED_ONCE:
                console.log("Permission denied");
                if (deniedCount < 3) {
                    console.log("in count" + deniedCount);
                    deniedCount++;
                    requestAuthorization();
                } else {
                    // Are we sure we want to hassle the user more than 3 times?
                }
                break;
            case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                console.log("Permission permanently denied");
                navigator.notification.confirm(
                "This app has been denied access to your camera and it really needs it function properly. Would you like to switch to the app settings page to allow access?",
                function (i) {
                    if (i == 1) {
                    cordova.plugins.diagnostic.switchToSettings();
                    }
                }, "Camera access denied", ['Yes', 'No']);
                break;
            case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                console.log("Permission granted always");
                // Yay! use camera
                break;
            }
        }
    
        function requestAuthorization() {
            cordova.plugins.diagnostic.requestCameraAuthorization(evaluateAuthorizationStatus, onError, false);
        }
    
        function checkAuthorization() {
            cordova.plugins.diagnostic.getCameraAuthorizationStatus(evaluateAuthorizationStatus, onError, false);
        }
    
        checkAuthorization();
    }
        // --- End servicenameController content ---
})();